ACP Control Time Calculator

This application takes in a series of distances, converts to either km or miles--the opposite of whatever is input.  

It also takes a start date and time.

It then uses the distance to calculate the open and close brevets for bikers to travel that distance.  It uses the formula found here: https://rusa.org/pages/acp-brevet-control-times-calculator

In the backend, this application uses AJAX and flask to dynamically update the page upon request.

The times are calculated in /brevets/acp_times.py. 

The flask backend is in flask_brevets.py.

