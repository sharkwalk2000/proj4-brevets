from acp_times import open_time, close_time
import nose
import arrow

def test_open_time():
    time1 = arrow.Arrow(
        2020, 1, 1, 0, 0
    )

    time2 = arrow.Arrow(
        2010, 11, 2, 2, 5
    )
    assert open_time(0, 200, time1.isoformat()) == '2020-01-01T08:00:00+00:00'

    assert open_time(60, 200, time1.isoformat()) == '2020-01-01T09:45:52.941176+00:00'

    assert open_time(110, 300, time2.isoformat()) == '2010-11-02T13:19:07.058824+00:00'

def test_close_time():
    time1 = arrow.Arrow(
        2020, 1, 1, 0, 0
    )

    time2 = arrow.Arrow(
        2020, 4, 29, 11, 25
    )

    assert open_time(0, 200, time1.isoformat()) == '2020-01-01T08:00:00+00:00'


    assert open_time(60, 200, time1.isoformat()) == '2020-01-01T09:45:52.941176+00:00'


    assert open_time(60, 300, time2.isoformat()) == '2020-04-29T21:10:52.941176+00:00'


