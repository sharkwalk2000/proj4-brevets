"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#



def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevets = {200 : 34, 300 : 34, 400 : 30, 600 : 28, 1000 : 26}
    ride_time = int(control_dist_km) / brevets[brevet_dist_km]
    date, time = brevet_start_time.split('T')
    year, month, day = date.split('-')
    hours, minutes, na1, na2 = time.split(':')      #dont need na1 or na2
    start = arrow.Arrow(
        int(year), int(month), int(day), int(hours), int(minutes)
    )
    t = start.shift(hours=+ride_time)
    t = t.shift(hours=+8)           #crude fix for timezone issues
    return t.isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    brevets = {200: 15, 300: 15, 400: 15, 600: 11.428, 1000: 13.333}
    ride_time = int(control_dist_km) / brevets[brevet_dist_km]
    date, time = brevet_start_time.split('T')
    year, month, day = date.split('-')
    hours, minutes, na1, na2 = time.split(':')  # dont need na1 or na2
    start = arrow.Arrow(
        int(year), int(month), int(day), int(hours), int(minutes)
    )
    t = start.shift(hours=+ride_time)
    t = t.shift(hours=+8)           #crude fix for timezone issues
    return t.isoformat()